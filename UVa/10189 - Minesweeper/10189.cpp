#include <stdio.h>
#include <unordered_map>
#include <vector>
#include <tuple>
#include <string>

using namespace std;

unordered_map<int, vector<int>> getField(int n, int m) {
    // Retrieve indexes where mines are located

    unordered_map<int, vector<int>> mines;

    for (int i = 0; i < n; i++) {
        vector<int> idx;
        char row[m];
        scanf("%s", row);

        for (int j = 0; j < m; j++) {
            if (row[j] == '*')
                idx.push_back(j);
        }

        if (!idx.empty()) {
            mines[i] = idx;
        }
    }

    return mines;
}

vector<vector<int>> add(int i, int j, vector<vector<int>> field) {
    // Calcular las 8 celdas a cambiar
    tuple<int, int> cell1(i + 1, j);
    tuple<int, int> cell2(i + 1, j + 1);
    tuple<int, int> cell3(i, j + 1);
    tuple<int, int> cell4(i - 1, j + 1);
    tuple<int, int> cell5(i - 1, j);
    tuple<int, int> cell6(i - 1, j - 1);
    tuple<int, int> cell7(i, j - 1);
    tuple<int, int> cell8(i + 1, j - 1);
    vector<tuple<int, int>> cells = {cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8};
    vector<tuple<int, int>> filtered_cells;

    // Filtrar las que son correctas
    for (int k = 0; k < 8; k++) {
        int ci, cj;
        tie(ci, cj) = cells.at(k);  // Unpack elements

        if ((ci >= 0) && (cj >= 0) && (ci < field.size()) && (cj < field[ci].size()) && (field.at(ci).at(cj) != -1))
            filtered_cells.push_back(cells.at(k));
    }

    // Sumar 1
    for (int k = 0; k < filtered_cells.size(); k++) {
        int ci, cj;
        tie(ci, cj) = filtered_cells[k];  // Unpack elements

        field[ci][cj]++;
    }

    return field;
}

vector<vector<int>> countMines(int n, int m, unordered_map<int, vector<int>> mines) {
    // Crear una matrix de NxM con ceros
    vector<vector<int>> counts;
    for (int i = 0; i < n; i++) {
        vector<int> row;
        for (int j = 0; j < m; j++) {
            row.push_back(0);
        }
        counts.push_back(row);
    }

    for (auto it : mines) {
        int i = it.first;
        vector<int> column = it.second;

        for (int j = 0; j < column.size(); j++) {
            // Sustituir el valor por mina
            counts.at(i).at(column.at(j)) = -1;

            // Sumar 1 a todos los circundantes
            counts = add(i, column.at(j), counts);
        }
    }

    return counts;
}

int main() {
    int n, m;
    int n_field = 1;
    bool exit = false;
    while (not exit) {
        // Recibir la primera línea
        if (scanf("%d %d", &n, &m) == 2) {
            if ((n == 0) && (m == 0)) {
                // End-of-file
                exit = true;
            } else {
                if (n_field > 1) printf("\n");
                printf("Field #%d:\n", n_field);

                // Get fields
                vector<vector<int>> counts;
                unordered_map<int, vector<int>> mines;
                mines = getField(n, m);
                counts = countMines(n, m, mines);

                // Escribir el mensaje
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        if (counts[i][j] == -1)
                            printf("*");
                        else
                            printf("%d", counts[i][j]);
                    }
                    printf("\n");
                }
                n_field++;
            }
        }
    }

    return 0;
}
