#! /usr/bin/env python3

import sys


def get_field(n, m):
    # Retrieve indexes where mines are located
    mines = {}

    for i in range(n):
        row = input()

        mines_ = [j for j in range(m) if row[j] == "*"]
        if len(mines_):
            mines[i] = mines_

    return mines


def add(i, j, field):
    # Calcular las 8 celdas a cambiar
    cells = [(i + 1, j), (i + 1, j + 1), (i, j + 1), (i - 1, j + 1), (i - 1, j), (i - 1, j - 1), (i, j - 1),
             (i + 1, j - 1)]

    # Filtrar las que son correctas
    cells = filter(lambda x: x[0] >= 0 and x[1] >= 0 and x[0] < len(field) and x[1] < len(field[x[0]]) and field[x[0]][
        x[1]] != "*", cells)

    # Sumar 1
    for i, j in cells:
        field[i][j] += 1

    return field


def count_mines(n, m, mines):
    counts = [[0 for _ in range(m)] for _ in range(n)]

    for i, columns in mines.items():
        for j in columns:
            # Sustituir por mina
            counts[i][j] = "*"

            # Sumar 1 a todos los circundantes
            counts = add(i, j, counts)

    return counts


if __name__ == '__main__':
    output = sys.stdout
    msg = []

    salir = False
    n_field = 1
    while not salir:
        try:
            field_msg = "Field #{0}:\n".format(n_field)
            inst = input()

            if len(inst) > 0:
                # Check if first: 0 < n, m ≤ 100
                nm = inst.split(" ")
                if len(nm) == 2:
                    n, m = nm
                else:
                    n, m = list(filter(lambda x: x, inst))

                n, m = int(n), int(m)

                if (n, m) != (0, 0):
                    mines = get_field(n, m)
                    counts = count_mines(n, m, mines)

                    # Write message
                    field_msg += "\n".join(["".join([str(counts[i][j]) for j in range(m)]) for i in range(n)])
                    msg.append(field_msg + "\n")

                    n_field += 1
                else:
                    # End-of-input
                    salir = True

        except EOFError:
            salir = True

    output.write("\n".join(msg))
    output.write("")
    output.close()
