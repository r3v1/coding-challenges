#! /usr/bin/env python3

import sys

if __name__ == '__main__':
    output_ = sys.stdout
    msg = ""

    is_first = True

    salir = False
    while not salir:
        try:
            inst = input()
            corrected = ""
            for i in range(len(inst)):
                if inst[i] == '"':
                    if is_first:
                        corrected += "``"
                        is_first = False
                    else:
                        corrected += "''"
                        is_first = True
                else:
                    corrected += inst[i]

            msg += "{0}\n".format(corrected)

        except EOFError:
            salir = True

    output_.write(msg)
    output_.write("")
    output_.close()