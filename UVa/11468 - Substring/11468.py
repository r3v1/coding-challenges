#! /usr/bin/env python3
import operator
import sys

from functools import reduce
from itertools import product


def prod(iterable):
    return reduce(operator.mul, iterable, 1)


def substring(alphabet, l_out, patterns):
    # Generate words
    words = list(map(''.join, product(alphabet.keys(), repeat=l_out)))

    filtered_words = []
    for word in words:
        if all(pattern not in word for pattern in patterns):
            filtered_words.append(word)

    prob = sum([prod([alphabet[char] for char in word]) for word in filtered_words])

    return prob


def word_prob(alphabet, word):
    return prod([alphabet[char] for char in word])


def parse_regex(alphabet, patterns, l_out):
    parsed = []
    for pattern in patterns:
        if len(pattern) <= l_out:
            for i in range(l_out - len(pattern) + 1):
                p = {i: set() for i in range(l_out)}
                for j, char in enumerate(pattern):
                    p[i + j].add(char)
                parsed.append(p)

    # Fill empty sets
    yes_words = set()
    for p in parsed:
        for k, v in p.items():
            if not v:
                p[k] = set(alphabet.keys())

        # Generate words
        yes_words.update(set(map("".join, product(*p.values()))))

    yes_prob = sum([prod([alphabet[char] for char in word]) for word in yes_words])
    no_prob = 1 - yes_prob

    return no_prob


if __name__ == '__main__':
    fptr = sys.stdout

    results = []

    # Input
    test_cases = int(input())

    for test in range(test_cases):
        n_patterns = int(input())
        patterns = []
        for _ in range(n_patterns):
            patterns.append(input())
        patterns.sort(key=lambda x: len(x), reverse=False)

        # Number of chars in alphabet
        n_chars = int(input())
        alphabet = {}
        for _ in range(n_chars):
            c = input().split(" ")
            alphabet[c[0]] = float(c[1])

        # Length of output string
        l_out = int(input())

        # prob = substring(alphabet, l_out, set(patterns))
        prob = parse_regex(alphabet, set(patterns), l_out)
        # results += ["Case #{0}: {1:.6f}\n".format(test+1, prob)]
        fptr.write("Case #{0}: {1:.6f}\n".format(test + 1, prob))

        # Blank line
        input()

    # Output
    # for result in results:
    #     fptr.write(result)
    fptr.write("")
    fptr.close()
