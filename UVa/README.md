# [Online Judge](https://onlinejudge.org/index.php)
##### Universidad de Valladolid coding challenge

### Problems solved

| Problem | Language | Submitted | Run Time |
|---------|:--------:|:---------:|:--------:|
| [100 - The 3n+1 Problem](100%20-%20The%203n+1%20Problem/100.pdf) | Python 3.5.1 | 16-10-2021 | 0.720 |
| [100 - The 3n+1 Problem](100%20-%20The%203n+1%20Problem/100.pdf) | Java 1.8 | 09-04-2020 | 0.400 |
| [100 - The 3n+1 Problem](100%20-%20The%203n+1%20Problem/100.pdf) | C++11 | 16-10-2021 | 0.050 |
| [101 - The Blocks Problem](101%20-%20The%20Blocks%20Problem/101.pdf) | Python 3.5.1 | 09-04-2020 | 0.020 |
| [272 - TEX Quotes](272%20-%20TEX%20Quotes/272.pdf) | Python 3.5.1 | 16-10-2021 | 0.020 |
| [10189 - Minesweeper](10189%20-%20Minesweeper/10189.pdf) | Python 3.5.1 | 17-10-2021 | 0.020 |
| [10189 - Minesweeper](10189%20-%20Minesweeper/10189.pdf) | C++11 | 18-10-2021 | 0.000 |



