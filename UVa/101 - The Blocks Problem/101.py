#! /usr/bin/env python3
import sys


def move_back(l):
    for item in l:
        idx = positions[item]
        world[idx].remove(item)
        world[item].append(item)
        positions[item] = item


def move_onto(a, b):
    """
    Puts block a onto block b after returning any blocks that are
    stacked on top of blocks a and b to their initial positions.

    Parameters
    ----------
    a: int
        Block numbers
    b: int
        Block numbers

    Returns
    -------
    dict
        Blocks world

    """
    # Return back a
    a_idx = positions[a]
    a_split = world[a_idx].index(a)
    move_back(world[a_idx][a_split + 1:])

    # Return back b
    b_idx = positions[b]
    b_split = world[b_idx].index(b)
    move_back(world[b_idx][b_split + 1:])

    # Move
    world[b_idx].insert(b_split + 1, world[a_idx][a_split])
    world[a_idx].remove(a)
    positions[a] = positions[b]


def move_over(a, b):
    # Return back a
    a_idx = positions[a]
    a_split = world[a_idx].index(a)
    move_back(world[a_idx][a_split + 1:])

    # Move
    b_idx = positions[b]
    world[b_idx].append(a)
    world[a_idx].remove(a)
    positions[a] = positions[b]


def pile_onto(a, b):
    # Return back b
    b_idx = positions[b]
    b_split = world[b_idx].index(b)
    move_back(world[b_idx][b_split + 1:])

    # Pile
    a_idx = positions[a]
    a_split = world[a_idx].index(a)
    world[b_idx].extend(world[a_idx][a_split:])
    world[a_idx] = world[a_idx][:a_split]

    # Update positions
    for i in world[b_idx]:
        positions[i] = b_idx


def pile_over(a, b):
    # Pile
    a_idx = positions[a]
    a_split = world[a_idx].index(a)
    b_idx = positions[b]
    world[b_idx].extend(world[a_idx][a_split:])
    world[a_idx] = world[a_idx][:a_split]

    # Update positions
    for i in world[b_idx]:
        positions[i] = b_idx


if __name__ == '__main__':
    fptr = sys.stdout

    n = int(input())
    positions = {i: i for i in range(n)}
    world = {i: [i] for i in range(n)}

    salir = False
    while not salir:
        inst = input()
        if inst == 'quit':
            salir = True
        else:
            inst = inst.split(' ')
            a = int(inst[1])
            b = int(inst[3])
            if a != b and positions[a] != positions[b]:
                if inst[0] == 'move':
                    if inst[2] == 'over':
                        move_over(a, b)
                    else:
                        move_onto(a, b)
                elif inst[0] == 'pile':
                    if inst[2] == 'over':
                        pile_over(a, b)
                    else:
                        pile_onto(a, b)

    for i in range(n):
        if world[i]:
            fptr.write("{0}: {1}\n".format(i, ' '.join(map(str, world[i]))))
        else:
            fptr.write("{0}:\n".format(i))
    fptr.write("")
    fptr.close()
