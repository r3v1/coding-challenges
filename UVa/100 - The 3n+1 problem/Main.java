import java.util.*;
import java.io.*;
import java.util.HashMap;

public class Main {
    private static HashMap<Integer, Integer> cache = new HashMap<Integer, Integer>();

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        try {
            while (true) {
                int first = scan.nextInt();
                int second = scan.nextInt();
                int start, end;

                // translate bounds as provided into lower & upper bound
                if (first < second) {
                    start = first;
                    end = second;
                } else {
                    start = second;
                    end = first;
                }

                // loop through range and store max
                int max = 0;
                int collatz = 0;
                for (int i = start; i <= end; i++) {
                    collatz = collatz(i);
                    if (collatz > max)
                        max = collatz;
                }

                // Output lower bound, upper bound, and max on a line
                System.out.printf("%d %d %d\n", first, second, max);
            }
        } catch (Exception e) {}
    }

    public static int collatz(int n) {
        if (!cache.containsKey(n)) {
            int c;
            if (n == 1) c = 1;
            else {
                if (n % 2 == 1) c = 1 + collatz(3 * n + 1);
                else c = 1 + collatz(n / 2);
            }
            cache.put(n, c);
        }

        return cache.get(n);
    }
}