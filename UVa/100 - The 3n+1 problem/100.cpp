#include <stdio.h>
#include <unordered_map>
using namespace std;

std::unordered_map<int, int> m;

int collatz(int i) {
    if (i > 1) {
      if (i % 2 == 0)
        return 1 + collatz(i / 2);
      else
        return 1 + collatz(1 + 3 * i);
    }

    return 1;
}

int main() {
  int a, b;
  m[0] = 0;
  m[1] = 1;

  while ((scanf("%d %d", &a, &b)) == 2) {
    int x = (a < b) ? a : b;
    int y = (a > b) ? a : b;

    int max_cycles = 0;
    for (int i = x; i <= y; i++) {
      // check if key is present
      if (m.find(i) == m.end())
        m[i] = collatz(i);

      if (m[i] > max_cycles)
        max_cycles = m[i];
    }

    printf("%d %d %d\n", a, b, max_cycles);
  }

  return 0;
}
