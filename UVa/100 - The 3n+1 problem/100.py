#! /usr/bin/env python3

import sys


def cycle_length(n):
    global cycles

    if n not in cycles.keys():
        if n % 2 == 1:
            c = 1 + cycle_length(3 * n + 1)
        else:
            c = 1 + cycle_length(n // 2)
        cycles[n] = c
    else:
        c = cycles[n]

    return c


if __name__ == '__main__':
    output_ = sys.stdout
    cycles = {0: 0, 1: 1}
    msg = ""

    salir = False
    while not salir:
        try:
            inst = input()

            if len(inst) > 0:
                inst = inst.split(' ')
                inst = list(filter(lambda x: x, inst))
                i = int(inst[0])
                j = int(inst[1])
                o = 1 if i < j else -1
                maximum = 0

                for n in range(i, j + o, o):
                    m = cycle_length(n)

                    if maximum < m:
                        maximum = m

                msg += "{0} {1} {2}\n".format(i, j, maximum)

        except EOFError:
            inst = ""
            salir = True

    output_.write(msg)
    output_.write("")
    output_.close()
